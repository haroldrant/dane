/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Negocio;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import ufps.util.colecciones_seed.ListaCD;
import ufps.Modelo.*;
import ufps.util.colecciones_seed.ColaP;
import ufps.util.colecciones_seed.Pila;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author MADARME
 */
public class Dane {

    private ListaCD<Region> regiones = new ListaCD();
    private ListaCD<Persona> personasErroneas = new ListaCD();

    public Dane() {
    }

    public Dane(String url) {
        this.leerUrLDane(url);
    }

    public ListaCD<Region> getRegiones() {
        return regiones;
    }

    public void setRegiones(ListaCD<Region> regiones) {
        this.regiones = regiones;
    }

    /*
    
REGION,CÓDIGO DANE DEL DEPARTAMENTO,DEPARTAMENTO,CÓDIGO DANE DEL MUNICIPIO,MUNICIPIO
Región Eje Cafetero - Antioquia,5,Antioquia,5001,Medellín
    
     */
    private void leerUrLDane(String url) {

        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        int codigoRegion = 1;
        for (int i = 1; i < v.length; i++) {

            String registro = v[i].toString();
            //Región Eje Cafetero , 5,Antioquia,5001,Medellín
            String datoRegion[] = registro.split(",");
            /*
            datoRegion[0]=Región Eje Cafetero
            datoRegion[1]=5
            datoRegion[2]= Antioquia
            datoRegion[3]=5001
            datoRegion[4]=Medellín
            
             */

            Region nuevaRegion = new Region(codigoRegion, datoRegion[0]);
            Region buscar = this.buscarRegion(nuevaRegion);
            if (buscar == null) {
                this.regiones.insertarAlFinal(nuevaRegion);
                codigoRegion++;
                buscar = nuevaRegion;
            }
            this.crearDepartamento(buscar, datoRegion);

        }
    }

    private void crearDepartamento(Region r, String datoRegion[]) {
        /*
            datoRegion[0]=Región Eje Cafetero
            datoRegion[1]=5
            datoRegion[2]= Antioquia
            datoRegion[3]=5001
            datoRegion[4]=Medellín
            
         */

        Departamento nuevo = new Departamento(Integer.parseInt(datoRegion[1]), datoRegion[2]);
        Departamento buscado = r.buscarDpto(nuevo);
        if (buscado == null) {
            r.getDptos().insertarAlFinal(nuevo);
            buscado = nuevo;
        }

        this.crearMunicipio(buscado, datoRegion);

    }

    private void crearMunicipio(Departamento d, String datoRegion[]) {

        /*
            datoRegion[0]=Región Eje Cafetero
            datoRegion[1]=5
            datoRegion[2]= Antioquia
            datoRegion[3]=5001
            datoRegion[4]=Medellín
            
         */
        Municipio nuevo = new Municipio(Integer.parseInt(datoRegion[3]), datoRegion[4]);
        d.getMunicipios().insertarAlFinal(nuevo);
    }

    private Region buscarRegion(Region nueva) {
        if (this.regiones.esVacia()) {
            return null;
        }
        for (Region r : this.regiones) {
            if (r.equals(nueva)) {
                return r;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Region r : this.regiones) {
            msg += "\n***********************************************************************\n";
            msg += "Region:" + r.getNombre() + ": Código de región:" + r.getCodigo() + "\n";
            for (Departamento d : r.getDptos()) {
                msg += "Departamento:" + d.getNombre() + " Código dpto:" + d.getCodigo() + "\n";

                for (Municipio m : d.getMunicipios()) {
                    msg += "Municipio:" + m.getNombre() + "\n";

                    //Cuando este proceso se realice , se borra la cola de personas
                    //::::::: ADVERTENCIA  ::::::::::::
                    ColaP<Persona> municipioClon = m.getPersonas().clonar();
                    while (!municipioClon.esVacia()) {
                        msg += "\n Persona:" + m.getPersonas().deColar().toString();
                    }
                }

            }
        }
        return msg;
    }

    public String getListadoRegiones() {
        String msg = "";
        for (Region r : this.regiones) {
            msg += "Region:" + r.getNombre() + ": Código de región:" + r.getCodigo() + " , Cantidad de subsidios:" + r.getCantidadBeneficiarios() + "\n";
        }
        return msg;

    }

    //Punto 2. 
    public void cargarSubsidioRegion(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();

        for (int i = 1; i < v.length; i++) {

            String registro = v[i].toString();
            /**
             * codigo_region;cantidad de subsidios 1;3 2;3 3;2 4;3 5;3 6;4
             */
            String datoRegion[] = registro.split(";");
            int codRegion = Integer.parseInt(datoRegion[0]);
            int canSub = Integer.parseInt(datoRegion[1]);
            //Suponemos que el archivo NO CONTIENE DATOS ERRADOS
            this.regiones.get(codRegion - 1).setCantidadBeneficiarios(canSub);
        }
    }

    //Punto 3.
    public void cargarPersonas(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String registro = v[i].toString();
            /*
                cedula;fecha;nombre;email;direccion;genero(0=mujer, 1=hombre);codigo_municipio
                4068079;13/01/63;pepe1;pepe1@email.com;calle 1 - av 0;1;54001
             */
            String persona[] = registro.split(";");
            Municipio buscado = buscarMunicipio(persona[6]);
            //Crear objeto persona
            Persona p = crearPersona(persona);
            //Verificar si existe la persona en el municipio, si tiene todos sus datos válidos y es menor a 100 años.
            Boolean personaValida = validarPersona(p);
            Boolean existePersona = existePersona(buscado, p);
            if (buscado != null && personaValida && !existePersona) {
                buscado.getPersonas().enColar(p, p.getEdad());
            } else {
                personasErroneas.insertarAlFinal(p);
            }
        }
    }

    private Persona crearPersona(String persona[]) {
        DateTimeFormatter formato = this.asignarFormato(persona[1]);
        LocalDate fecha = LocalDate.parse(persona[1], formato);
        Boolean genero = ("1".equals(persona[5]));
        Persona p = new Persona(Long.valueOf(persona[0]), fecha, persona[2],
                persona[3], persona[4], genero);
        return p;
    }

    private Boolean existePersona(Municipio m, Persona p) {
        ColaP<Persona> personas = m.getPersonas().clonar();
        while (!personas.esVacia()) {
            Persona persona = personas.deColar();
            if (persona.equals(p)) {
                return true;
            }
        }
        return false;
    }

    private Boolean validarPersona(Persona p) {
        if (p.getNombres() != null && p.getEmail() != null && p.getCedula() != 0
                && p.getDireccion() != null && p.getFechaNacimiento() != null && p.getEdad() < 100) {
            return true;
        }
        return false;
    }

    private DateTimeFormatter asignarFormato(String formato) {
        DateTimeFormatter format;
        if (formato.length() == 7) {
            format = new DateTimeFormatterBuilder()
                    .appendPattern("d/MM/")
                    .optionalStart()
                    .appendPattern("yyyy")
                    .optionalEnd()
                    .optionalStart()
                    .appendValueReduced(ChronoField.YEAR, 2, 2, 1900)
                    .optionalEnd()
                    .toFormatter();
        } else {
            format = new DateTimeFormatterBuilder()
                    .appendPattern("dd/MM/")
                    .optionalStart()
                    .appendPattern("yyyy")
                    .optionalEnd()
                    .optionalStart()
                    .appendValueReduced(ChronoField.YEAR_OF_ERA, 2, 2, 1900)
                    .optionalEnd()
                    .toFormatter();
        }
        return format;
    }

    private Municipio buscarMunicipio(String codigo) {
        String codigoDpto = dividirCodigo(codigo);
        for (Region r : this.regiones) {
            Departamento d = r.buscarDpto(Integer.parseInt(codigoDpto));
            if (d != null) {
                Municipio m = d.buscarMunicipio(Integer.parseInt(codigo));
                if (m != null) {
                    return m;
                }
            }
        }
        return null;
    }

    private String dividirCodigo(String codigo) {
        if (codigo.length() == 4) {
            return codigo.substring(0, 1);
        } else {
            return codigo.substring(0, 2);
        }
    }

    //Punto 4.a
    public int procesarSubsidios() {
        int subsidios = 0;
        for (Region r : this.regiones) {
            subsidios += r.getSubsidiosPersonas();
        }
        return subsidios;
    }

    //Punto 4.b
    public int getCantidadSubsidioDepartamentos() {
        int contadorDpto = 0;
        for (Region r : this.regiones) {
            contadorDpto += r.getSubsidioDpto();
        }
        return contadorDpto;
    }

    //Punto 4.c    
    public ListaCD<Persona> getPersonasNoSubsidio() {
        ListaCD<Persona> personasNoSubsidio = new ListaCD();
        for (Region r : this.regiones) {
            ListaCD<Persona> personasTemporales = r.getPersonasNoSubsidio();
            for (Persona p : personasTemporales) {
                personasNoSubsidio.insertarAlFinal(p);
            }
        }
        return personasNoSubsidio;
    }

    //Punto 4.d    
    public Pila<Region> getSubisidiosRegion() {
        ListaCD<Region> subsidiosRegion = new ListaCD();
        Pila<Region> subsidios = new Pila();
        for (Region r : this.regiones) {
            subsidiosRegion.insertarOrdenado(r);
        }
        for (int i = 0; i < subsidiosRegion.getTamanio(); i++) {
            subsidios.apilar(subsidiosRegion.get(i));
        }
        return subsidios;
    }

    //Punto 4.e
    public String getDatosPersona(long cedula) {
        for (Region r : this.regiones) {
            String datos = r.getDatosPersona(cedula);
            if (datos != null) {
                return datos += r.getNombre();
            }
        }
        return "No existe esta persona";
    }

    // Punto 4.f
    public int getCantidadSubsidioMunicipio(int codMunicipio) {
        int cantidadMunicipio = 0;
        Municipio m = buscarMunicipio(Integer.toString(codMunicipio));
        if (m != null) {
            ColaP<Persona> personas = m.getPersonas().clonar();
            while (!personas.esVacia()) {
                Persona persona = personas.deColar();
                if (persona.isSiEsBeneficiario()) {
                    cantidadMunicipio++;
                }
            }
        }
        return cantidadMunicipio;
    }

    //Punto 4.g
    public ListaCD<Persona> getDatosErroneos() {
        return this.personasErroneas;
    }
}
