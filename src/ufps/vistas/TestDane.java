/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.Negocio.Dane;

/**
 *
 * @author MADARME
 */
public class TestDane {
    public static void main(String[] args) {
        String url="http://ufps30.madarme.co/dptoDane.csv";
        Dane dane=new Dane(url);
        //System.out.println(dane.toString());
        
        dane.cargarSubsidioRegion("http://ufps30.madarme.co/persistencia/subsidioregion.txt");
        dane.cargarPersonas("http://ufps30.madarme.co/persistencia/dane_personas.txt");
        //System.out.println(dane.getListadoRegiones());
        System.out.println("Cantidad de Subsidios entregados: \n");
        System.out.println(dane.procesarSubsidios());
        System.out.println("Cantidad de subsidios por departamento: \n");
        System.out.println(dane.getCantidadSubsidioDepartamentos());
        System.out.println("Personas que no tienen subsidios \n");
        System.out.println(dane.getPersonasNoSubsidio().toString());
        System.out.println("Subsidios ordenados por región de manera descendente: \n");
        System.out.println(dane.getSubisidiosRegion().toString());
        System.out.println("Datos de una persona por su cédula: ");
        System.out.println(dane.getDatosPersona(60880996));
        System.out.println("Cantidad de subsidios por municipio especificado: \n");
        System.out.println(dane.getCantidadSubsidioMunicipio(68001));
        System.out.println("Listado de personas con datos erroneos o con edad mayor a 100 años: \n");
        System.out.println(dane.getDatosErroneos().toString());
    }    
}
