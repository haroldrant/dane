/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.ColaP;

/**
 *
 * @author MADARME
 */
public class TestColaPrioridad {
    
    public static void main(String[] args) {
        
        Cola<String> nombres=new Cola();
        
        nombres.enColar("madarme");
        nombres.enColar("Juan");
        nombres.enColar("Javier");
        nombres.enColar("Genesis");
        nombres.enColar("Cristian");
        
        while(!nombres.esVacia())
            System.out.println(nombres.deColar());
        /*
            Juan --> 10
            Cristian-->10
            Javier -->5
            Genesis-->3
            madarme-->0
        */
        
        
    }
    
}
