/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Modelo;

import java.util.Objects;
import ufps.util.colecciones_seed.ColaP;
import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author MADARME
 */
public class Region implements Comparable<Region> {
    
    private int codigo; //Se va a generar
    private String nombre;
    private ListaCD<Departamento> dptos=new ListaCD();
    private int cantidadBeneficiarios=0;
   

    public Region() {
    }

    public Region(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public int getCantidadBeneficiarios() {
        return cantidadBeneficiarios;
    }

    public void setCantidadBeneficiarios(int cantidadBeneficiarios) {
        this.cantidadBeneficiarios = cantidadBeneficiarios;
    }    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    @Override
    public String toString() {
        return "Region{" + "codigo=" + codigo + ", nombre=" + nombre + ", Cantidad Beneficiarios: "+ cantidadBeneficiarios+'\n';
    }
    
    public Departamento buscarDpto(Departamento d)
    {
        //Reduncia:
        //Por definición:
        if(this.dptos.esVacia())
            return null;    
        for(Departamento dpto:this.dptos)
        {
            if(dpto.equals(d))
                return dpto;
        }
        return null;
    }
    
    public Departamento buscarDpto(int d)
    {  
        for(Departamento dpto:this.dptos)
        {
            if(dpto.getCodigo()==d)
                return dpto;
        }
        return null;
    }
    
    @Override
    public int compareTo(Region other){
        if(this.cantidadBeneficiarios < other.getCantidadBeneficiarios()){
            return -1;
        }
        if(this.cantidadBeneficiarios > other.getCantidadBeneficiarios()){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Region other = (Region) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
    public int getSubsidiosPersonas(){
        int subsidios=0;
        for(Departamento d: this.dptos){
            ColaP<Persona> p = d.getSubsidiosPersonas();
            while(!p.esVacia() && this.cantidadBeneficiarios>subsidios){
                Persona persona = p.deColar();
                if(persona.getEdad()>40){
                    persona.setSiEsBeneficiario(true);
                    subsidios++;
                }
            }
        }
        return subsidios;
    }
    
    public ListaCD<Persona> getPersonasNoSubsidio(){
        ListaCD<Persona> personasTotal = new ListaCD();
        for(Departamento d: this.getDptos()){
            ListaCD<Persona> personaDpto = d.getPersonas();
            for(Persona p: personaDpto){
                if(!p.isSiEsBeneficiario()){
                    personasTotal.insertarAlFinal(p);
                }
            }
        }
        return personasTotal;
    }
    
    public int getSubsidioDpto(){
        int contador=0;
        for(Departamento d:this.dptos){
            int subsidios = d.getSubsidiosDpto();
            contador+=subsidios;
        }
        return contador;
    }
    
    public String getDatosPersona(long cedula){
        for(Departamento d: this.dptos){
            String datos = d.getDatosPersona(cedula);
            if(!datos.equals("")){
                return datos+="Departamento: " + d.getNombre()+'\n';
            }
        }
        return null;
    }
}